import os

from django.urls import path

from .views import ProductDetail, ProductList

app_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))


urlpatterns = [
    path("<int:pk>/", ProductDetail.as_view(), name="ProductDetail"),
    path("", ProductList.as_view(), name="all"),
]
