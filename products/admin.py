from django.contrib import admin

from core.models import Picture

from .models import Category, Product


class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = "related_obj"
    extra = 0


def clear_category(self, request, queryset):
    for obj in queryset:
        obj.categories.clear()
    messages.info(request, "категории удалены")


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = (PictureInline,)
    list_display = ("title", "price", "maker", "sort", "active")
    list_editable = ("active", "sort")
    actions = (clear_category,)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(active=True)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""

    inlines = (PictureInline,)
