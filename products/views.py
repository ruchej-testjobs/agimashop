from django.shortcuts import render
from .models import Product
from django.views import generic


class ProductList(generic.ListView):
    model = Product
    context_object_name = "products"


class ProductDetail(generic.DetailView):
    model = Product
    context_object_name = "product"
