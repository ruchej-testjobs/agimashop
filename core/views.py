from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from django.views import generic
from products.views import ProductList
from .models import Baner, Brand


class Index(ProductList):
    template_name = "core/index.html"
    extra_context = {
        "page_title": _("Магазин"),
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["baners"] = Baner.objects.all()
        context["brands"] = Brand.objects.all()
        return context
