import os
from django.urls import path
from . import views

app_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))

urlpatterns = [
    path("", views.Index.as_view(), name="index"),
]
