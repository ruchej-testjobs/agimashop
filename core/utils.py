import os

from django.conf import settings
from PIL import ExifTags, Image, ImageEnhance


def rotate_by_exif(image):
    """Rotates the picture by information from EXIF, if any
    image - picture object 

    """
    if hasattr(image._getexif(), "items") is False:
        return image
    for orient in ExifTags.TAGS.keys():
        if ExifTags.TAGS[orient] == "Orientation":
            break
    exif = dict(image._getexif().items())
    if exif.get(orient, 0) == 3:
        image = image.rotate(180, expand=True)
    elif exif.get(orient, 0) == 6:
        image = image.rotate(270, expand=True)
    elif exif.get(orient, 0) == 8:
        image = image.rotate(90, expand=True)
    return image


def get_box_thumb(image, TIS):
    """
    image - the object of the picture on which you overlay the image
    TIS - THUMB_IMAGE_SIZE (50, 50)

    """
    img_w, img_h = image.size
    min_size = min(img_w, img_h)
    left = int((img_w - min_size) / 2)
    upper = int((img_h - min_size) / 2)
    box = (left, upper, left + min_size, upper + min_size)
    image = image.crop(box)
    image.thumbnail(TIS, resample=Image.LANCZOS)
    return image


def get_rename_path(path, newname):
    """Returns the path to the renamed file"""

    ext = os.path.splitext(path)[1]
    dirpath = os.path.dirname(path)
    return "{0}/{1}{2}".format(dirpath, newname, ext)


def get_prefix_path(path, prefix="tmb"):
    """Returns the path to the preview"""

    split = os.path.splitext(path)
    dirname, ext = split
    thumb_file_name = "{0}_{1}{2}".format(dirname, prefix, ext)
    return thumb_file_name


def to_webp(path, name):
    """Convert a picture to a WebP format"""

    dirname = os.path.dirname(path)
    newpath = os.path.join(dirname, "tmp.webp")

    dirname = os.path.dirname(name)
    newname = os.path.join(dirname, "tmp.webp")

    img = Image.open(path)
    img = rotate_by_exif(img)
    img.save(newpath, "WEBP", quality=90)
    os.remove(path)

    return newname


def get_jpg_path(path, folder="JPG"):
    """Get JPG File Path"""

    dir, filename = os.path.split(path)
    newdir = os.path.join(dir, folder)
    name, ext = os.path.splitext(filename)
    newfilename = "{0}.{1}".format(name, "jpg")
    newpath = os.path.join(newdir, newfilename)

    return newpath


def to_jpg(path, folder="JPG"):
    """Save a picture in jpg format to the specified folder"""

    dir, filename = os.path.split(path)
    newdir = os.path.join(dir, folder)
    if not os.path.exists(newdir):
        os.mkdir(newdir)

    newpath = get_jpg_path(path, folder)
    img = Image.open(path)
    img = rotate_by_exif(img)
    if img.mode == "RGBA":
        background = Image.new("RGBA", img.size, (242, 242, 242))
        img = Image.alpha_composite(background, img)
        img = img.convert("RGB")
    img.save(newpath, quality=100)
    return newpath
