# -*- coding: utf-8 -*-

from django.conf import settings

from .models import Menu

MENU = Menu.objects.all()


def get_version(request):
    return {"VERSION": settings.VERSION}


def get_menus(request):
    return {"menu": MENU}
