import os

import PIL
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from . import utils


class Core(models.Model):
    class Meta:
        ordering = ("sort", "title")
        verbose_name = _("Ядро")
        verbose_name_plural = _("Ядра")

    title = models.CharField(
        _("заголовок объекта"), max_length=250, blank=True, null=True
    )
    description = models.TextField(_("описание объекта"), blank=True, null=True)
    sort = models.IntegerField(
        _("номер объекта для сортировки"), default=0, blank=True, null=False
    )
    active = models.BooleanField(_("Активен ли объект"), default=True, db_index=True)

    def __str__(self):
        return f"{self.title}" if self.title else ""

    def delete(self, **kwargs):
        if "force" in kwargs:
            super().delete()
        else:
            self.active = False
            self.save()

    def get_verbose_name(self):
        return self._meta.verbose_name

    @classmethod
    def get_model_name(cls):
        return cls.__name__.lower()

    @classmethod
    def get_app_name(cls):
        return cls._meta.app_label.lower()

    def get_absolute_url(self):
        return reverse(
            f"{self.get_app_name()}:{self.get_model_name().capitalize()}Detail",
            args=[self.pk],
        )

    def get_picture(self):
        for image in self.pictures.all():
            return image

    def thumb(self):
        pic = self.get_picture()
        if pic:
            return pic.thumb()
        else:
            return "---"


class PictureManager(models.QuerySet):
    def get_queryset(self, **kwargs):
        return self.filter(Q(active=True) | Q(related_obj__isnull=True))

    def get_count(self):
        return self.count()


class Picture(Core):
    class Meta:
        ordering = ("sort", "title")
        verbose_name = _("Картинка")
        verbose_name_plural = _("Картинки")

    image = models.ImageField(upload_to="pictures")
    related_obj = models.ForeignKey(
        Core,
        verbose_name=_("Привязан к объекту"),
        null=True,
        blank=True,
        related_name="pictures",
        on_delete=models.CASCADE,
    )

    objects = PictureManager.as_manager()

    @classmethod
    def get_count_all(cls):
        return cls.objects.get_count()

    def delete(self, **kwargs):
        self.related_obj = None
        super().delete(**kwargs)

    def get_jpg_url(self):

        return utils.get_jpg_path(self.image.url)

    def get_thumb_url(self):
        return utils.get_prefix_path(self.image.url)

    def get_thumb_jpg_url(self):

        return utils.get_jpg_path(self.get_thumb_url())

    def get_middle_thumb_url(self):
        return utils.get_prefix_path(self.image.url, "mid")

    def get_middle_thumb_jpg_url(self):

        return utils.get_jpg_path(self.get_middle_thumb_url())

    def thumb(self):
        if self.image:
            path = self.get_thumb_url()
            return format_html('<img src="{0}">', path)
        else:
            return "---"

    thumb.short_description = "Картинка"

    def save(self, *args, **kwargs):
        # At first - usual preservation
        super().save(*args, **kwargs)

        # Check if picture is specified
        if self.image:

            self.image.name = utils.to_webp(self.image.path, self.image.name)
            filepath = self.image.path
            new_db_img = utils.get_rename_path(self.image.name, self.pk)
            new_file_name = utils.get_rename_path(filepath, self.pk)
            thumb_file_name = utils.get_prefix_path(new_file_name)
            middle_file_name = utils.get_prefix_path(new_file_name, "mid")
            # Let's create a preview
            image = PIL.Image.open(filepath)
            image = utils.get_box_thumb(image, settings.THUMB_IMAGE_SIZE)
            image.save(os.path.normpath(thumb_file_name))
            utils.to_jpg(thumb_file_name)
            # Let's create a cover
            image = PIL.Image.open(filepath)
            image.thumbnail(settings.MIDDLE_SIZE_IMAGE, resample=PIL.Image.LANCZOS)
            image.save(os.path.normpath(middle_file_name))
            utils.to_jpg(middle_file_name)
            # Simply reduce the picture to the specified MAX_SIZE_IMAGE
            image = PIL.Image.open(filepath)
            width, height = image.size
            if (width > settings.MAX_SIZE_IMAGE[0]) or (
                height > settings.MAX_SIZE_IMAGE[1]
            ):
                image.thumbnail(settings.MAX_SIZE_IMAGE, resample=PIL.Image.LANCZOS)
            image.save(os.path.normpath(new_file_name))
            self.image.name = new_db_img
            super().save(*args, **kwargs)
            image.close()
            utils.to_jpg(self.image.path)
            if filepath != new_file_name:
                os.remove(filepath)


class MenuManager(models.QuerySet):
    """docstring for MenuManager"""

    def get_menu(self, attr):
        for menu in self.filter(title=attr, parent_id__isnull=True):
            return menu

    def get_supermenu(self):
        return self.filter(parent_id__isnull=True)

    def __getattr__(self, attr):
        attr, splitter, menu = attr.partition("get_menu_")
        if splitter:
            return self.get_menu(menu)
        return super().__getattr__(attr)


class Menu(Core):
    class Meta:
        ordering = ("parent_id", "sort", "title")
        verbose_name = _("Элемент меню")
        verbose_name_plural = _("Меню сайта")

    url = models.CharField(
        _("Линк"), max_length=256, blank=False, null=False, default="index"
    )
    css_class = models.CharField(
        _("CSS-Класс блока меню"), max_length=30, null=False, blank=True, default=""
    )
    seen_gasts = models.BooleanField(
        _("Элемент меню виден незарегистрированным пользователям"), default=True
    )
    seen_users = models.BooleanField(
        _("Элемент меню виден зарегистрированным пользователям"), default=False
    )
    parent = models.ForeignKey(
        "self",
        verbose_name=_("Суперкласс Меню"),
        null=True,
        blank=True,
        related_name="submenus",
        on_delete=models.CASCADE,
    )
    objects = MenuManager.as_manager()

    def __str__(self):
        self.title = self.title or self.css_class
        return super().__str__()

    def get_title(self):
        return super().__str__()

    def set_css_active(self):
        self.css_class = f"active {self.css_class}"
        return self

    def get_url(self):
        url = self.url
        return reverse(url) if ":" in url else url


class ActiveManager(models.QuerySet):
    def get_queryset(self, **kwargs):
        return self.filter(Q(active=True) | Q(related_obj__isnull=True))


class Baner(Core):
    class Meta:
        ordering = ("sort", "title")
        verbose_name = _("Банер")
        verbose_name_plural = _("Банеры")

    url = models.URLField(_("Ссылка банера"), max_length=200)
    objects = ActiveManager.as_manager()


class Brand(Core):
    class Meta:
        ordering = ("sort", "title")
        verbose_name = _("Бренд")
        verbose_name_plural = _("Бренды")

    objects = ActiveManager.as_manager()
